<?php

$headers = [
    'account_id',
    'authorizing_id',
    'authorizing_type',
    'call_direction',
    'call_forwarded',
    'call_id',
    'callee_id_name',
    'callee_id_number',
    'caller_id_name',
    'caller_id_number',
    'custom_application_vars',
    'custom_channel_vars',
    'emergency_resource_used',
    'from',
    'inception',
    'is_internal_leg',
    'local_resource_id',
    'local_resource_used',
    'other_leg_call_id',
    'owner_id',
    'request',
    'reseller_id',
    'timestamp',
    'to',
    'hook_event',
    'hangup_cause',
    'hangup_code',
    'duration_seconds',
    'ringing_seconds',
    'billing_seconds',
];

$csvPath = '../data.csv';

$content = file_get_contents('php://input');
$data = json_decode($content, true);

if (!file_exists($csvPath)) {
    file_put_contents($csvPath, implode(',', array_keys($data)) . "\n");
}

$h = fopen($csvPath, 'a');

$result = array_map(function($item){
    return is_array($item) ? json_encode($item) : $item;
}, $data);

fputcsv($h, $result);
fclose($h);

echo "OK";
