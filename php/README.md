# PHP Webhook Listener

This is a naive PHP webhook listener in old-school vanilla PHP. A real world project should use a proper web server such as nginx/apache.

## Prequisite

- PHP >= 5.6

## Run

```bash
php -S localhost:3000
```
