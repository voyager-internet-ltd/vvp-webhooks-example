# Webhook

## What is a webhook

Webhooks are automated messages sent when an event happens. They can be configured with one or more listeners which will receive and process these messages in an asynchronous manner.

## Use Cases

In the context of voice, webhooks can be sent from the following events:

- **Call Parking**: a call gets parked, or retrieved from a parking slot or abandoned.
- **Channel Answer**: a channel establishes two-way audio, such as a voicemail box or the called party answering.
- **Channel Bridge**: two channels are bridged together, such as two users/devices connected together.
- **Channel Create**: a new channel is created.
- **Channel Destroy**: when a channel is destroyed, usually as a result of a hangup.
- **Notifications**: this will fire when a notification event is triggered in Kazoo.
- **Object**: when objects (like JSON document objects) of certain types in Kazoo are changed.
- **SMS**

In each case, a message containing detailed information about the particular event will be sent out.

## The Webhook App

Choose the webhook app from the menu:
![webhook app](screenshots/apps-menu.png "Webhook app launcher")

The main app page shows the status of all currently configured webhooks:
![webhook main page](screenshots/main-page.png "Webhook main page")

To configure a webhook, click the "New Webhook" botton or click edit on an existing webhook. This will open up the form for configuration:
![webhook configuration](screenshots/configuration.png "Webhook configuration")

The `URL` field is the url for the listener endpoint that will process this particular webhook.

## Example Listeners

The following example apps currently exist to help demonstrate the usage.

- [javascript](javascript)
- [php](php)
- [python](python)

In each example, a `Channel Destroy` message can be consumed and saved into a local csv file.

To run and test the example apps, go into each app directory and refer to the simple startup guide.

### Testing

A `sample.json` file exists to help test the webhook listeners.

From the root directory:

```bash
curl -X POST -d "@sample.json" -H "Content-Type: application/json" localhost:3000
```

This will post a channel destroy event to the webhook listener.

Do it a few times. A `data.csv` file should appear under the root directory that contains the posted data.
