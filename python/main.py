import time
import listener
import signal
import csv

csv_columns = [
    "account_id",
    "authorizing_id",
    "authorizing_type",
    "call_direction",
    "call_forwarded",
    "call_id",
    "callee_id_name",
    "callee_id_number",
    "caller_id_name",
    "caller_id_number",
    "custom_application_vars",
    "custom_channel_vars",
    "emergency_resource_used",
    "from",
    "inception",
    "is_internal_leg",
    "local_resource_id",
    "local_resource_used",
    "other_leg_call_id",
    "owner_id",
    "request",
    "reseller_id",
    "timestamp",
    "to",
    "hook_event",
    "hangup_cause",
    "hangup_code",
    "duration_seconds",
    "ringing_seconds",
    "billing_seconds",
]

csv_file = "../data.csv"


class Worker:
    run = True

    def __init__(self):
        self.webhooks = listener.Listener(
            handlers={"POST": self.process_post_request})
        signal.signal(signal.SIGINT, self.handle_shutdown)
        signal.signal(signal.SIGTERM, self.handle_shutdown)

    def run(self):
        try:
            with open(csv_file, 'w', newline='', encoding='utf-8') as csvfile:
                writer = csv.DictWriter(csvfile, fieldnames=csv_columns)
                writer.writeheader()
        except IOError:
            print("I/O error")

        self.webhooks.start()

        while self.run:
            pass

    def process_post_request(self, data, *args, **kwargs):
        print("Request received: %s" % data)

        try:
            with open(csv_file, 'a+', newline='', encoding='utf-8') as csvfile:
                writer = csv.DictWriter(csvfile, fieldnames=csv_columns)
                writer.writerow(data)
        except IOError:
            print("I/O error")

        return

    def handle_shutdown(self, signum, frame):
        print("Shutting down...")
        self.webhooks.stop()
        self.run = False


if __name__ == '__main__':
    worker = Worker()
    worker.run()
