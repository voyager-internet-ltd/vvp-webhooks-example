# Python Webhook Listener

This is a naive implemention of a Python webhook listener using `cherrypy`.

## Prerequisite

- Python
- Pip

## Setup

Install dependencies:

```bash
pip install -r requirements.txt
```

## Run

Start the listner:

```bash
python main.py
```

This is now running at localhost:3000
