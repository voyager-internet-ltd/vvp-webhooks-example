# Javascript Webhook Listener

This is a very naive implementation of a Javascript webhook listener in `express`.

## Development

### Prerequisite

- Node version >= 12
- npm

### Setup

Install dependencies

```bash
npm i
```
