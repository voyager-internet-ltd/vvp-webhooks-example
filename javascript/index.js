const express = require('express')
const bodyParser = require('body-parser')
const fs = require('fs')
const app = express()
app.use(bodyParser.json());
const port = 3000

const headers = [
    'account_id',
    'authorizing_id',
    'authorizing_type',
    'call_direction',
    'call_forwarded',
    'call_id',
    'callee_id_name',
    'callee_id_number',
    'caller_id_name',
    'caller_id_number',
    'custom_application_vars',
    'custom_channel_vars',
    'emergency_resource_used',
    'from',
    'inception',
    'is_internal_leg',
    'local_resource_id',
    'local_resource_used',
    'other_leg_call_id',
    'owner_id',
    'request',
    'reseller_id',
    'timestamp',
    'to',
    'hook_event',
    'hangup_cause',
    'hangup_code',
    'duration_seconds',
    'ringing_seconds',
    'billing_seconds',
]

const csvPath = '../data.csv'
// Ensure file exists and clear content
fs.closeSync(fs.openSync(csvPath, 'w'))
// Write headers first
fs.writeFileSync(csvPath, headers.join(','))

app.post('/', (req, res) => {
    const data = req.body

    fs.appendFileSync(csvPath, "\n" + Object.values(data).join(','))

    res.send('OK')
})

app.listen(port, () => {
    console.log(`Webhook listener running at http://localhost:${port}`)
})
